$(function() {
    $("[data-toggle='tooltip']").tooltip();
    $("[data-toggle='popover']").popover();
    $('.carousel').carousel({
        interval: 2000
    });

    $("#contacto").on("shown.bs.modal", function(e) {
        console.log("el modal se mostró");
    });
    $("#contacto").on("show.bs.modal", function(e) {
        console.log("el modal se está mostrando");

        $("#contactoBtn").removeClass("btn-outline-success");
        $("#contactoBtn").addClass("btn-primary");
        $("#contactoBtn").prop("disabled", true);
    });
    $("#contacto").on("hide.bs.modal", function(e) {
        console.log("el modal se está ocultando");
    });
    $("#contacto").on("hidden.bs.modal", function(e) {
        console.log("el modal se ocultó");

        $("#contactoBtn").addClass("btn-outline-success");
        $("#contactoBtn").removeClass("btn-primary");
        $("#contactoBtn").prop("disabled", false);
    });
});